Pod::Spec.new do |s|
s.name             = 'iFlutter'
s.version          = '1.0.0.debug'
s.summary          = 'Umbrella contains all frameworks needed to run Flutter components'
s.description      = <<-DESC
						Código flutter embebido en un framework
                    DESC
s.homepage         = 'https://flutter.dev/docs/development/tools/sdk/releases?tab=macos'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'iOS Native Team' => 'iOSMobile.group@bbva.com' }
s.source           = { :git => 'https://bitbucket.org/MILATeam/iflutter.git', :tag => s.version.to_s }
s.frameworks = 'Foundation'
s.swift_version = '4.2'
s.platforms = { :ios => "10.0" }
#s.ios.vendored_frameworks = 'output/iFlutter.framework'
s.ios.source_files = 'iFlutter/iFlutter/Code/**/*'

s.subspec 'Flutter' do |f|
    f.vendored_frameworks = 'iFlutter/output/outputDebug/Flutter.framework'
end

s.subspec 'App' do |a|
    a.vendored_frameworks = 'iFlutter/output/outputDebug/App.framework'
    a.source_files = 'iFlutter/output/outputDebug/FlutterPluginRegistrant/**/*.{h,m}'
end

end
