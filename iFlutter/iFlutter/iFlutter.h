//
//  iFlutter.h
//  iFlutter
//
//  Created by Pilar Millán on 25/09/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for iFlutter.
FOUNDATION_EXPORT double iFlutterVersionNumber;

//! Project version string for iFlutter.
FOUNDATION_EXPORT const unsigned char iFlutterVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <iFlutter/PublicHeader.h>


