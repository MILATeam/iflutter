//
//  iFlutterDelegateBuilder.swift
//  iFlutter
//
//  Created by Pilar Millán on 24/06/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import UIKit

public class iFlutterDelegateBuilder {
    public static func build() -> iFlutterDelegate {
        return iFlutterDelegateImpl()
    }
}
