//
//  iFlutterDelegate.swift
//  iFlutter
//
//  Created by Pilar Millán on 24/06/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import UIKit
import Flutter

public protocol iFlutterDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
}

class iFlutterDelegateImpl: FlutterPluginAppLifeCycleDelegate {
    let engine: FlutterEngine
    let lifeCycleDelegate: FlutterPluginAppLifeCycleDelegate
    
    override init() {
        lifeCycleDelegate = FlutterPluginAppLifeCycleDelegate()
        engine = FlutterEngine(name: "io.flutter", project: nil)
        super.init()
    }
}

extension iFlutterDelegateImpl: iFlutterDelegate {
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]?) -> Bool {
        engine.run(withEntrypoint: nil)
        return lifeCycleDelegate.application(application, didFinishLaunchingWithOptions: launchOptions ?? [:])
    }
}
