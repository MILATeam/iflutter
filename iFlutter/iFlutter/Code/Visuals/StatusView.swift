//
//  StatusView.swift
//  iFlutter
//
//  Created by Pilar Millán on 24/06/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import UIKit
import Flutter

public protocol StatusView: class {
    var currentView: UIView { get }
}

extension StatusView where Self: UIViewController {
    public var currentView: UIView {
        return view
    }
}

extension FlutterViewController: StatusView {
    
}

public class StatusViewComponent {
    static var component: StatusView = FlutterViewController()
    
    public static var instance: StatusView {
        let vc = FlutterViewController()
        vc.setInitialRoute("Status view component")
//        StatusViewComponent.component = vc

        return vc//StatusViewComponent.component
    }
    
    public static func view(text: String) -> StatusView {
        let vc = FlutterViewController()
        vc.setInitialRoute(text)
        
        return vc
    }
}
