//
//  StatusViewController.swift
//  iFlutter
//
//  Created by Pilar Millán on 24/06/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import UIKit
import Flutter

public class StatusViewController: FlutterViewController {
    public static var viewController: StatusViewController {
        let vc = StatusViewController()
        vc.setInitialRoute("Status view controller")
        return vc
    }
}
