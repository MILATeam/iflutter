# iFlutter

Creación de un framework que embeba el todo el código necesario para poder ejecutar el proyecto Flutter en una app de iOS

* **Flutter version**: [v.1.5.4-hotfix.2](https://flutter.dev/docs/development/tools/sdk/releases?tab=macos)

* Documentación:
    * [Tutorial](https://github.com/flutter/flutter/wiki/Add-Flutter-to-existing-apps)
    * [Flutter](https://github.com/flutter/flutter)

##  Requerimientos

* iOS 10.0 o superior

## Uso

* Definir en `AppDelegate` una propiedad de tipo `iFlutterDelegate`:

```swift

class AppDelegate: UIResponder, UIApplicationDelegate {
	...
	let iFlutterDelegate: iFlutterDelegate
}
```

Sobreescribir el inicializador de AppDelegate para crear la instancia de `iFlutterDelegate `:

```swift
override init() {
	iFlutterDelegate = iFlutterDelegateBuilder()
	super.init()
}
```

Por cada método del ciclo de vida de la aplicación será necesario invocar al método análogo de `iFlutterDelegate ` tal que:

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        ...
        return iFlutterDelegate.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
```

* `StatusView`

Para utilizar este componente, solamente tendríamos que crear una instancia de la siguiente forma:

```swift
let statusView = StatusViewComponent.component
addSuview(statusView)
``` 

* `StatusViewController`

Para crear una instancia de este view controller bastaría con:

```swift
let viewController = StatusViewController.viewController
```

## Conclusiones

* Se han seguido los pasos para integrar Flutter en una app existente del siguiente post: https://medium.com/flutter-community/add-flutter-to-existing-android-ios-app-ae8c4fb1582e

* Para poder generar el iFlutter.framework necesitamos exponer los frameworks de Flutter, FluterPluginRegistered y App.framework. Sin estas dependencias el código expuesto de esta librería
no va a compilar. Flutter y FluterPluginRegistered son dependencias propias que hay que exponer, y App.framework contiene los recursos necesarios para poder lanzar el código escrito en Dart.

* De momento solamente he podido exportar el framework y probar en dispositivo, en simulador sigue dando error de arquitectura, no encuentra compilación para i386. Nota: Para poder solventar este error de compilador,
hay que meter un post-procesado en el Podfile del consumidor de la librería para modificar la configuración del proyecto, de forma que para configuración _Debug_ se establezca `ONLY_ACTIVE_ARCH` a `NO`, mientras que para
configuraciones _Release_ tiene que tener el valor `YES`.

* El script de Flutter del target iFlutter tiene que ejecutarse siempre, por tanto hay que desmarcar _Run script only when installing_ para que se pueda compilar el código Dart y generar el nuevo el proyecto iOS de Flutter.

* Se ha probado su integración tanto en dispositivo como en simulador con varios modelos y el rendimiento es bueno, así como la apariencia. La única pega es que simulador de iPhone 5 no funciona porque el framework de _Flutter_ no admite arquitectura i386.
Esto no debería ser un problema de cara a la integración continua, ya que esta utiliza la última versión de iOS para lanzar los tests unitarios. A menos que queramos probar sobre una versión de iOS en un simulador iPhone 5, este inconveniente es menor.

* Aumento de aproximadamente 35MB el peso del proyecto.

### Actualizado a 19-septiembre

Ha cambiado la forma de integrar Flutter en una app existente, por tanto hay que volver a seguir los pasos indicados en: https://github.com/flutter/flutter/wiki/Add-Flutter-to-existing-apps

Para actualizar la forma en que se añade flutter al proyecto se han seguido los siguientes pasos: https://github.com/flutter/flutter/wiki/Upgrading-Flutter-added-to-existing-iOS-Xcode-project
   
   * Con esta nueva actualización, desaparece la ejecución de los scripts en 'Build phases' para volver a compilar la app en Flutter.
   
   * También cambia la forma de instalar las librerías a través del podile:
        
        flutter_application_path = 'path/to/my_flutter/'
        load File.join(flutter_application_path, '.ios', 'Flutter', 'podhelper.rb')
        
        For each Xcode target that needs to embed Flutter, call install_all_flutter_pods(flutter_application_path).
        
        target 'MyApp' do
        install_all_flutter_pods(flutter_application_path)
        end
        
        target 'MyAppTests' do
        install_all_flutter_pods(flutter_application_path)
        end
        
   * Para poder utilizra la librería hay que exponer 3 frameworks: App Flutter y FluterPluginRegistrant, y para ello lo que hacemos es ejecutar un target universal que construye en modo release para todas las arquitecturas, 
    y exponer en el .podspec como vendored frameworks: App y FluterPluginRegistrant, y el código fuente que contiene la librería. Pero esto sigue sin funcionar ya que da problemas para exportar en modo release para actiquecturas de simulador, ya que flutter no soporta esta configuración: release para x64_86
    
   * Si se exporta todo en modo release, al intentar instalar sobre un dispositivo se obtiene el siguiente error: Error while initializing the Dart VM: Wrong full snapshot version, expected 'a1aaab2706cefc32dd1df8e973509540' found 'c8562f0ee0ebc38ba217c7955956d1cb'
    y probamos a cambiar el origin de Flutter: pasamos de la rama stable a master: `flutter channel master`,  a continuación actualizamos Flutter `flutter upgrade`  y limpiamos `flutter clean`. A continuación volver a construir el proyecto Flutter.
    Después de esto tenemos el mismo error, y es porque no se está utilizando la misma versión del framework de Flutter para el desarrollo de los módules de flutter que la versión expuesta en el podspec que en este caso ha sido la versión colgada en cocoapods: se estaba exponiendo una dependencia en lugar de un vendored framework. Por tanto, volviendo a establecer la dependencia de Flutter como un vendored framwork conseguimos compilar la librería e instalar la en la aplicación cliente lanzándola en un dispositivo físico.
    La configuración utilizada ha sido la siguiente:
    
        * Ejecución target iFlutter-Universal SOLAMENTE para dispositivo físico
        * Exponer en el podspec como `vendored frameworks` lo siguiente:
            `s.ios.vendored_frameworks = 'output/FlutterPluginRegistrant.framework', 'output/Flutter.framework', 'output/App.framework'`
        * En la app cliente, este caso BBVA, añadir una regla de post-instalación en el Podfile tal que:
        
```
        post_install do |installer|
            installer.pods_project.targets.each do |target|
                target.build_configurations.each do |config|
                   
                   if target.name == 'iFlutter'
                    if config.name == 'Market' || config.name == 'HackingEtico'
                        config.build_settings['ONLY_ACTIVE_ARCH'] = 'NO'
                    else
                        config.build_settings['ONLY_ACTIVE_ARCH'] = 'YES'
                    end
                end
            end
        end
```
El script utilizado en el target universal para la generación del framework ha sido el siguiente:

```
UNIVERSAL_OUTPUTFOLDER=${PROJECT_DIR}/output
FLUTTER_APP="../my_flutter/.ios/Flutter/App.framework"
FLUTTER_FOLDER="../my_flutter/.ios/Flutter/engine/Flutter.framework"

set -e

if [ -d "${UNIVERSAL_OUTPUTFOLDER}" ]; then
rm -rf "${UNIVERSAL_OUTPUTFOLDER}"
fi

# Make sure the output directory exists
mkdir -p "${UNIVERSAL_OUTPUTFOLDER}"

# Next, work out if we're in SIM or DEVICE
xcodebuild -workspace "${PROJECT_NAME}".xcworkspace -scheme "${PROJECT_NAME}" -configuration Release -arch arm64 -arch armv7 only_active_arch=yes defines_module=yes -sdk "iphoneos"

# Step 2. Copy the framework structure (from iphoneos build) to the universal folder
cp -R "${BUILD_DIR}/Release-iphoneos/${PROJECT_NAME}.framework" "${UNIVERSAL_OUTPUTFOLDER}/"

# Step 3. Copy Swift modules from iphonesimulator build (if it exists) to the copied framework directory
BUILD_PRODUCTS="${SYMROOT}"

# Step 4. Create universal binary file using lipo and place the combined executable in the copied framework directory
lipo -create -output "${UNIVERSAL_OUTPUTFOLDER}/${PROJECT_NAME}.framework/${PROJECT_NAME}"  "${BUILD_DIR}/Release-iphoneos/${PROJECT_NAME}.framework/${PROJECT_NAME}"

#Step 5. Copy FlutterPluginRegistrant framework to the universal folder
cp -R "${BUILD_DIR}/Release-iphoneos/FlutterPluginRegistrant/FlutterPluginRegistrant.framework" "${UNIVERSAL_OUTPUTFOLDER}/"
cp -R "${FLUTTER_FOLDER}" "${UNIVERSAL_OUTPUTFOLDER}/"
cp -R "${FLUTTER_APP}" "${UNIVERSAL_OUTPUTFOLDER}/"

```

Eliminando esta regla de post-instalación en el podfile de la aplicación cliente, también ha funcionado la librería, mostrando el componente Status en la pantalla de Login. Sin embargo, para poder compilar y ejecutar en simulador, estas reglas de post-instalación SON NECESARIAS.

La versión de Flutter utilizada para que la ejecución en un dispositivo físico tuviese éxito:
>
    Flutter 1.10.5-pre.27 • channel master • https://github.com/flutter/flutter.git
    Framework • revision c94a994d62 (89 minutes ago) • 2019-09-19 09:44:31 -0700
    Engine • revision 1cada0a6dc
    Tools • Dart 2.6.0 (build 2.6.0-dev.0.0 ea969c358e)
    
Sin embargo, aunque para simulador compila y ejecuta, no se visualzan los módulos de Flutter, y la consola lanza los sigueintes errores:

>**Failed to find snapshot**: /Users/pilarmillan/Library/Developer/CoreSimulator/Devices/F131EAD2-044F-4304-AE7A-D6F2F2B96B21/data/Containers/Bundle/Application/0CD34E10-D6F8-494B-91E4-C8B45E4FB4CB/BBVAWoody.app/Frameworks/App.framework/flutter_assets/kernel_blob.bin
2019-09-19 20:43:08.151942+0200 BBVAWoody[4966:5932341] [VERBOSE-2:engine.cc(124)] Engine run configuration was invalid.
2019-09-19 20:43:08.152240+0200 BBVAWoody[4966:5932341] [VERBOSE-2:shell.cc(404)] Could not launch engine with configuration.

* **[Viernes, 20 septiembre, 2019]**

Cambiamos la forma de exponer los frameworks necesarios para poder ejecutar iFlutter, en este caso `App, Flutter y FlutterPluginRegistrant`. Primero de todo, eliminamos el target _Universal_ y añadimos una nueva _Build phase_, y la situamos la última de todas. Es un script que copia estos frameworks al directorio `dependencies` de la librería iFlutter:

```
# Type a script or drag a script file from your workspace to insert its path.
UNIVERSAL_OUTPUTFOLDER=${PROJECT_DIR}/dependencies
FLUTTER="../my_flutter/.ios/Flutter"
FLUTTER_APP="${FLUTTER}/App.framework"
FLUTTER_APP_PODSPEC="${FLUTTER}/my_flutter.podspec"
FLUTTER_FOLDER="${FLUTTER}/engine/Flutter.framework"
FLUTTER_PODSPEC="${FLUTTER}/engine/Flutter.podspec"
PLUGIN_FOLDER="${FLUTTER}/FlutterPluginRegistrant"

set -e

if [ -d "${UNIVERSAL_OUTPUTFOLDER}" ]; then
    rm -rf "${UNIVERSAL_OUTPUTFOLDER}"
fi

# Make sure the output directory exists
mkdir -p "${UNIVERSAL_OUTPUTFOLDER}"

cp -R "${FLUTTER_FOLDER}" "${UNIVERSAL_OUTPUTFOLDER}/"
cp -R "${FLUTTER_PODSPEC}" "${UNIVERSAL_OUTPUTFOLDER}/"
cp -R "${FLUTTER_APP}" "${UNIVERSAL_OUTPUTFOLDER}/"
cp -R "${FLUTTER_APP_PODSPEC}" "${UNIVERSAL_OUTPUTFOLDER}/"
cp -R "${PLUGIN_FOLDER}" "${UNIVERSAL_OUTPUTFOLDER}/"

```

A continuación, modificamos el fichero `iFlutter.podspec` para exponer los frameworks como _subspecs_ en lugar de como vendored. Por lo que el podspec queda tal que:

```
Pod::Spec.new do |s|
s.name             = 'iFlutter'
s.version          = '2.0.0'
s.summary          = 'iFlutter contains visuals written in Dart'
s.description      = <<-DESC
						Código flutter embebido en un framework
                    DESC
s.homepage         = 'https://flutter.dev/docs/development/tools/sdk/releases?tab=macos'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'iOS Native Team' => 'iOSMobile.group@bbva.com' }
s.source           = { :git => 'https://descinet.bbva.es/stash/scm/im/iflutter.git', :tag => s.version.to_s }
s.frameworks = 'Foundation'
s.swift_version = '4.2'
s.platforms = { :ios => "10.0" }
s.ios.source_files = 'iFlutter/Code/**/*'

    s.subspec 'App' do |app|
        app.vendored_frameworks = 'dependencies/App.framework'
    end
    
    s.subspec 'Flutter' do |flutter|
        flutter.vendored_frameworks = 'dependencies/Flutter.framework'
    end
    
    s.subspec 'Plugins' do |plugins|
        plugins.source_files = 'dependencies/FlutterPluginRegistrant/**/*.{h,m}'
    end
end
```
Tras la instalación en la app cliente, se hacen pruebas tanto en simulador como en dispositivo y los componentes en Flutter se ejecutan correctamente.

Se ha exportado un .ipa con la configuración de Market (con provisioning profile ENTERPRISE) y tras la instalación en un iPhone, la app no llega a inicia.

