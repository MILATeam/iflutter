//
//  ViewController.swift
//  DemoApp
//
//  Created by Pilar Millán on 25/09/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import UIKit
import Flutter

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vc = FlutterViewController()
        vc.setInitialRoute("Status view component")
        
        let statusView = vc.view!
        view.addSubview(statusView)
        statusView.translatesAutoresizingMaskIntoConstraints = false
        
        statusView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        statusView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        statusView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        statusView.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        addChild(vc)
    }
}

